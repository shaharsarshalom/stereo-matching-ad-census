#pragma once

#include "stdafx.h"
#include "StereoMaching_export.h"
#include "StereoMatchingConfig.h"
#include <vector>
#include <bitset>
#include <ppl.h>
#include <opencv2\core.hpp>
#include <opencv2\core\types.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <ImageUtility.h>
#include <Stopper.h>
#include "IStereoMatching.h"

using namespace std;
using namespace cv;
using namespace concurrency;


struct LeftToRightConsistency
{
	template <typename T = int>
	static inline T CalcPd(T d, T pixelLoc)
	{
		return pixelLoc - d;
	}

	template <typename T = int>
	static inline T GetStartColumn(T d, T cols)
	{
		return d;
	}

	template <typename T = int>
	static inline T GetEndColumn(T d, T cols)
	{
		return cols;
	}
};

struct RightToLeftConsistency
{
	template <typename T = int>
	static inline T CalcPd(T d, T pixelLoc)
	{
		return pixelLoc + d;
	}

	template <typename T = int>
	static inline T GetStartColumn(T d, T cols)
	{
		return 0;
	}

	template <typename T = int>
	static inline T GetEndColumn(T d, T cols)
	{
		return cols - d;
	}
};

template <int cw, int ch>
class StereoMatching : public IStereoMatching
{
public:
	StereoMatching(const StereoMatchingConfig& config = StereoMatchingConfig());
	virtual ~StereoMatching();

	void ComputeStereoMatching(const cv::Mat& left, const cv::Mat& right, cv::Mat& dispairyMap);
	void ComputeStereoMatchingMatlab(const uchar* leftPtr, const uchar* rightPtr, float* disparityMapPtr, int rows, int cols);

	void SetConfig(const StereoMatchingConfig& config)
	{
		m_config = config;
	}

	const StereoMatchingConfig& GetConfig() override {
		return m_config;
	}

	typedef std::vector<std::vector<std::tuple<cv::Point2i, cv::Point2i>>> AggregatedRegion;

private:
	// Threading local data, avoid forward declaration  
	struct ThreadLocalData
	{
		// Integral cost used for cross aggregation, size of (rows*2, cols)
		cv::Mat integralCost;

		// Auxiliary image size (rows, cols), used on scan line phase 
		cv::Mat auxImg;

		// Hold the minimum value from all costs images, e.g all disparity ranges, this used during the scan line phase 
		cv::Mat m_minCost;

		// Used in order to calculate the min cost from each pixel in multiple dispairy levels 
		std::vector<float> costValues;
	};

private:
	///////////////////////////////////////////////////////////////////
	// Execute AD census cost phase 
	template <typename T, typename SideConsistency, bool IsParallel, bool DebugFlag>
	void ADCensusCostInitialization(const cv::Mat& leftRGB, const cv::Mat& leftGray, const cv::Mat& rightRGB, const cv::Mat& rightGray, std::vector<cv::Mat>& cost, std::string direction);

	template <typename T, typename SideConsistency>
	void ADCensusCostInitialization(const cv::Mat& leftRGB, const cv::Mat& leftGray, const cv::Mat& rightRGB, const cv::Mat& rightGray, std::vector<cv::Mat>& cost, int d);

	// Useful functions for AD census computation
	inline float Rho(float cost, float lambda)
	{
		return 1.f - expf(-cost / lambda);
	}


	template <bool IsParallel>
	void CreateCensus(const cv::Mat& srcGray, std::vector<std::bitset<cw * ch>>& censusImg);

	void CreateCensus(const cv::Mat& srcGray, std::vector<std::bitset<cw * ch>>& censusImg, int r);

	template <typename T = float>
	inline T GetMaxCostValue()
	{
		// Max cost value, used much as max cost value during the AD census phase 
		//constexpr float MaxCostValue = 1000;
		//return MaxCostValue;
		return numeric_limits<T>::max();
	}
	///////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////
	// Calculate aggregated regions 

	template <typename T, bool IsParallel, bool DebugFlag>
	void CalcAggregatedRegions(const Mat& RGB, vector<vector<tuple<cv::Point2i, cv::Point2i>>>& aggregatedRegions)
	{
		// First vector represent each pixel in the image, second vector represent the start and end point in a line
		for (auto& i : aggregatedRegions)
		{
			i.resize(0);
		}

		if (IsParallel)
		{
			parallel_for(int(0), m_rows, [&](int r)
			{
				CalcAggregatedRegions<T, DebugFlag>(RGB, aggregatedRegions, r);
			});
		}
		else
		{
			for (auto r = 0; r < m_rows; r++)
			{
				CalcAggregatedRegions<T, DebugFlag>(RGB, aggregatedRegions, r);
			}
		}
	}

	template <typename T, bool DebugFlag>
	void CalcAggregatedRegions(const Mat& rgb, vector<vector<tuple<cv::Point2i, cv::Point2i>>>& aggregatedRegions, int r)
	{
		T Tau1 = m_config.Tau1;
		T Tau2 = m_config.Tau2;
		auto L1square = m_config.L1 * m_config.L1;
		auto L2square = m_config.L2 * m_config.L2;
		auto maxPixelDistance = m_config.L1;

		auto cols = m_cols;
		auto rows = m_rows;
		// Work on each pixel 
		auto rowLoc = r * m_cols;
		for (auto c = 0; c < cols; c++)
		{
			// Calculate the aggregated cross region for each pixel 
			auto& aggreRegion = aggregatedRegions[rowLoc + c];

			auto pixelCenterRGBvalue = rgb.at<cv::Vec3b>(r, c);

			// Aggregate up and origin pixel(x,y) line
			{
				auto rLimit = std::max(r - maxPixelDistance, 0), rShiftUp = r, rStart = r;
				bool isContinue = true;
				for (; isContinue && rShiftUp >= rLimit; rShiftUp--)
				{
					CalcAggregatedRegionsVertical<T>(c, r, rShiftUp, maxPixelDistance, cols, Tau1, Tau2, L1square, L2square, pixelCenterRGBvalue, rgb, aggreRegion, isContinue);
				}

				if (aggreRegion.size() == 0)
				{
					// Shahar Sar Shalom TODO: check validity of this action, there is infairness in the aggregate up, and aggregate down, maybe we need to divide to three sections 
					// In case no pixels has aggregated, we'll aggregate the origin pixel to the aggregation region 
					cv::Point2i point(c, r);
					aggreRegion.push_back(make_tuple(point, point));
				}
			}

			// Aggregate Down
			{
				auto rLimit = std::min(r + maxPixelDistance, rows), rShiftDown = r + 1, rStart = r + 1;
				bool isContinue = true;
				for (; isContinue && rShiftDown < rLimit; rShiftDown++)
				{
					CalcAggregatedRegionsVertical<T>(c, r, rShiftDown, maxPixelDistance, cols, Tau1, Tau2, L1square, L2square, pixelCenterRGBvalue, rgb, aggreRegion, isContinue);
				}
			}

			if (false && c % 30 == 0 && DebugFlag)
			{
				auto path = m_config.DebugPath + "AggregatedRegion" + "_r_" + to_string(r) + "_c_" + to_string(c) + "." + m_config.DebugExt;
				CImageUtility::dbg_writeImg_highlightRegion(rgb, aggreRegion, path);
			}
		}
	}


	template <typename T>
	inline void CalcAggregatedRegionsVerticalLine(int cShift, int cStart, int c_origin, int r_origin, int rShift, T Tau1, T Tau2, int L1square, int L2square,
		const cv::Vec3b& pixelCenterRGBvalue, const cv::Mat& rgb,
		cv::Vec3b& RGBvalueShiftedPre, bool& isContinue);

	template <typename T>
	inline void CalcAggregatedRegionsVertical(int c, int r, int rShift, int maxPixelDistance, int cols, T Tau1, T Tau2, int L1square, int L2square,
		const cv::Vec3b& pixelCenterRGBvalue, const cv::Mat& RGB,
		std::vector<std::tuple<cv::Point2i, cv::Point2i>>& aggregationRegion, bool& isContinue);

	template <typename T>
	inline T Dc(const cv::Vec3b& a, const cv::Vec3b& b);


	///////////////////////////////////////////////////////////////////
	// Execute the cross based aggregation step 
	template <typename T, int TopencvImgType, typename SideConsistency, bool IsParallel, bool DebugFlag>
	void CrossBasedCostAggregation(const AggregatedRegion& aggregatedRegions, std::vector<cv::Mat>& cost, std::string debugPath)
	{
		auto num_iterations = m_config.num_cost_aggregation_iterations, rows = m_rows, cols = m_cols;

		if (IsParallel)
		{
			parallel_for(int(0), m_config.DisparityRange, [&](int d)
			{
				CrossBasedCostAggregation<T, TopencvImgType, SideConsistency>(aggregatedRegions, cost[d], m_combinable.local().integralCost, num_iterations, rows, cols, d);
			});
		}
		else
		{
			for (auto d = 0; d < m_config.DisparityRange; d++)
			{
				CrossBasedCostAggregation<T, TopencvImgType, SideConsistency>(aggregatedRegions, cost[d], m_combinable.local().integralCost, num_iterations, rows, cols, d);
			}
		}

		if (DebugFlag && m_config.DebugMode)
		{
			auto path = m_config.DebugPath + "CrossBasedCostAggregation_" + debugPath + "." + m_config.DebugExt;
			cv::Mat dispairtyImg;

			CreateDispairtyImage(cost, dispairtyImg, m_combinable.local().costValues);
			CImageUtility::dbg_writeImg(dispairtyImg, path);
		}
	}

	// Update the cost by averaging the costs from aggregated regions 
	template <typename T, int TopencvImgType, typename SideConsistency>
	void CrossBasedCostAggregation(const AggregatedRegion& aggregatedRegions, cv::Mat& cost, cv::Mat& costIntegral, int num_iterations, int rows, int cols, int d)
	{
		typedef float IntegralPercision;
		constexpr auto IntegralPercisionImageType = CV_32FC1;

		// Compute the aggregation only on valid regions, this way we can save time and avoid invalid pixels values. 
		auto cValidStart = SideConsistency::GetStartColumn(d, cols);
		auto cValidEnd = SideConsistency::GetEndColumn(d, cols);
		auto cValidLenght = cValidEnd - cValidStart;
		auto cValidEndMinuesOne = cValidEnd - 1;

		// Calculate integral image for each line 
		auto integral_num_cols = cols + 1;
		costIntegral.create(rows * 2, integral_num_cols, IntegralPercisionImageType);
		assert(DebugMemorySet<T>(costIntegral));

		//run for n iterations
		for (auto i = 0; i < num_iterations; i++)
		{
			// Calculate integral 1d image for each line 
			for (auto r = 0; r < rows; r++)
			{
				Mat srcCost(1, cValidLenght, TopencvImgType, (T*)cost.data + cValidStart + r * cost.step / sizeof(T), cost.step);
				// we need to manually point and create a requested cost integral image size 
				Mat dstIntegralCostRow(2, cValidLenght + 1, IntegralPercisionImageType, (IntegralPercision*)costIntegral.data + cValidStart + 2 * r * costIntegral.step / sizeof(IntegralPercision), costIntegral.step);

				integral(srcCost, dstIntegralCostRow, IntegralPercisionImageType);
			}

			// Aggregated each pixel using mean 
			for (auto r = 0; r < rows; r++)
			{
				auto rowLoc = r * cols;
				// Aggregate only valid regions 
				for (auto c = cValidStart; c < cValidEnd; c++)
				{
					IntegralPercision sum = 0.f;
					int numElements = 0;
					const auto& aggreRegion = aggregatedRegions[rowLoc + c];

					for (const auto& v : aggreRegion)
					{
						// aggregated coordinates reside in range [0,N], the integral image is in range [0,N+1], therefor, we must change the start and end coordinates according to the following convention, [start', end'] --> [start, end + 1]
						const auto& from = std::get<0>(v);
						const auto& to = std::get<1>(v);

						cv::Point2i from_validRegion, to_validRegion;
						assert(from.y == to.y);
						from_validRegion.x = std::max(cValidStart, from.x);
						to_validRegion.x = std::min(cValidEndMinuesOne, to.x);

						from_validRegion.y = to_validRegion.y = to.y;

						// Do not aggregate invalid cost values, invalid cost values contain high cost value, these values may change the aggregation value dramatically. 
						auto integral_row = from_validRegion.y * 2 + 1;
						auto integral_to = to_validRegion.x + 1;
						auto s = costIntegral.at<IntegralPercision>(integral_row, from_validRegion.x);
						auto e = costIntegral.at<IntegralPercision>(integral_row, integral_to);
						assert(s != numeric_limits<T>::infinity() && e != numeric_limits<T>::infinity());
						sum += (e - s);
						numElements += integral_to - from_validRegion.x;
					}

					assert(numElements != 0);
					auto updatedCost = sum / IntegralPercision(numElements);
					assert(updatedCost < GetMaxCostValue<T>());
					cost.at<float>(r, c) = updatedCost;
				}
			}
		}
	}

	template <typename T>
	bool DebugMemorySet(cv::Mat& m);
	///////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////
	// Scan line optimization 

	template <typename T, int TopencvImgType, typename SideConsistency, bool IsParallel, bool DebugFlag>
	void ScanlineOptimization(std::vector<cv::Mat>& cost, const cv::Mat& leftRGB, const cv::Mat& rightRGB, std::vector<cv::Mat> costScanline[4], std::string direction)
	{
		if (IsParallel)
		{
			parallel_for(int(0), 4, [&](int r)
			{
				if (r == 0)
					ScanlineOptimizationButtomToTop<T, TopencvImgType, SideConsistency, IsParallel, DebugFlag>(cost, leftRGB, rightRGB, costScanline[3], direction);
				else if (r == 1)
					ScanlineOptimizationTopToButtom<T, TopencvImgType, SideConsistency, IsParallel, DebugFlag>(cost, leftRGB, rightRGB, costScanline[2], direction);
				else if (r == 2)
					ScanlineOptimizationRightToLeft<T, TopencvImgType, SideConsistency, IsParallel, DebugFlag>(cost, leftRGB, rightRGB, costScanline[0], direction);
				else if (r == 3)
					ScanlineOptimizationLeftToRight<T, TopencvImgType, SideConsistency, IsParallel, DebugFlag>(cost, leftRGB, rightRGB, costScanline[1], direction);
			});
		}
		else
		{
			ScanlineOptimizationButtomToTop<T, TopencvImgType, SideConsistency, IsParallel, DebugFlag>(cost, leftRGB, rightRGB, costScanline[3], direction);

			ScanlineOptimizationTopToButtom<T, TopencvImgType, SideConsistency, IsParallel, DebugFlag>(cost, leftRGB, rightRGB, costScanline[2], direction);

			ScanlineOptimizationRightToLeft<T, TopencvImgType, SideConsistency, IsParallel, DebugFlag>(cost, leftRGB, rightRGB, costScanline[0], direction);

			ScanlineOptimizationLeftToRight<T, TopencvImgType, SideConsistency, IsParallel, DebugFlag>(cost, leftRGB, rightRGB, costScanline[1], direction);
		}

		if (IsParallel)
		{
			parallel_for(int(0), m_config.DisparityRange, [&](int d)
			{
				for (auto scanDirection = 1; scanDirection < 4; scanDirection++)
				{
					if (scanDirection == 1)
					{
						cv::add(costScanline[0][d], costScanline[1][d], cost[d]);
					}
					else
					{
						cv::add(costScanline[scanDirection][d], cost[d], cost[d]);
					}
				}
				cv::multiply(cost[d], 0.25f, cost[d]);
			});
		}
		else
		{
			for (auto d = 0; d < m_config.DisparityRange; d++)
			{
				for (auto scanDirection = 1; scanDirection < 4; scanDirection++)
				{
					if (scanDirection == 1)
					{
						cv::add(costScanline[0][d], costScanline[1][d], cost[d]);
					}
					else
					{
						cv::add(costScanline[scanDirection][d], cost[d], cost[d]);
					}
				}
				cv::multiply(cost[d], 0.25f, cost[d]);
			}

		}

		if (DebugFlag && m_config.DebugMode)
		{
			cv::Mat dispairtyImg;
			CreateDispairtyImage(cost, dispairtyImg, m_combinable.local().costValues);
			CImageUtility::dbg_writeImg(dispairtyImg, m_config.DebugPath + "ScanlineOptimization_AllDirections_" + direction + "." + m_config.DebugExt);
		}
	}
	
	template <typename T, int TopencvImgType, typename SideConsistency, bool IsParallel, bool DebugFlag>
	void ScanlineOptimizationButtomToTop(std::vector<cv::Mat>& cost, const cv::Mat& leftRGB, const cv::Mat& rightRGB, std::vector<cv::Mat>& costScanline, std::string direction)
	{
		auto Pai1 = m_config.Pai1, Pai1_quarter = Pai1 / 4.f, Pai1_tenth = Pai1 / 10.f;
		auto Pai2 = m_config.Pai2, Pai2_quarter = Pai2 / 4.f, Pai2_tenth = Pai2 / 10.f;

		auto cols = m_cols, rows = m_rows;

		auto Tau_so = T(m_config.Tau_so);

		for (auto r = rows - 1; r >= 0; r--)
		{
			for (auto c = 0; c < cols; c++)
			{
				for (auto d = 0; d < m_config.DisparityRange; d++)
				{
					auto cValidStart = SideConsistency::GetStartColumn(d, cols);
					auto cValidEnd = SideConsistency::GetEndColumn(d, cols);

					auto& costScanlineDisparityD = costScanline[d];

					if (r == rows - 1 || c < cValidStart || c >= cValidEnd)
					{
						costScanlineDisparityD.at<T>(r, c) = cost[d].at<T>(r, c);
					}
					else
					{
						// pd is c location on right image 
						auto pd = SideConsistency::CalcPd(d, c);

						// C1(p,d)
						auto cost_d_r_c = cost[d].at<T>(r, c);

						// Cr(p-r,d)
						auto cost_d_r_cMinusOne = costScanlineDisparityD.at<T>(r + 1, c);

						// Cr(p-r, +-d)
						T cost_dMinusPlusOne_r_cMinusOne;
						if (d == 0)
							cost_dMinusPlusOne_r_cMinusOne = costScanline[d + 1].at<T>(r + 1, c);
						else if (d == (m_config.DisparityRange - 1))
							cost_dMinusPlusOne_r_cMinusOne = costScanline[d - 1].at<T>(r + 1, c);
						else
							cost_dMinusPlusOne_r_cMinusOne = std::min(costScanline[d - 1].at<T>(r + 1, c), costScanline[d + 1].at<T>(r + 1, c));

						// MINkCr(p-r,k)
						T cost_dMin_r_cMinusOne = std::min(cost_dMinusPlusOne_r_cMinusOne, cost_d_r_cMinusOne);
						for (auto dd = 0; dd < m_config.DisparityRange; dd++)
						{
							if (dd == (d - 1) || dd == (d + 1) || dd == d)
							{
								continue;
							}
							else
							{
								cost_dMin_r_cMinusOne = std::min(cost_dMin_r_cMinusOne, costScanline[dd].at<T>(r + 1, c));
							}
						}

						auto left_r_c = leftRGB.at<Vec3b>(r, c);
						auto left_r_cMinusOne = leftRGB.at<Vec3b>(r + 1, c);
						auto right_r_c = rightRGB.at<Vec3b>(r, pd);
						Vec3b right_r_cMinusOne;
						right_r_cMinusOne = rightRGB.at<Vec3b>(r + 1, pd);

						T P1, P2;
						// Left to right
						auto D1 = Dc<T>(left_r_c, left_r_cMinusOne);
						auto D2 = Dc<T>(right_r_c, right_r_cMinusOne);
						CalcP1P2<T>(D1, D2, Tau_so, Pai1, Pai2, Pai1_tenth, Pai2_tenth, Pai1_quarter, Pai2_quarter, P1, P2);

						T leftToRight = cost_d_r_c + std::min(cost_d_r_cMinusOne, std::min(cost_dMinusPlusOne_r_cMinusOne + P1, cost_dMin_r_cMinusOne + P2)) - cost_dMin_r_cMinusOne;

						costScanlineDisparityD.at<T>(r, c) = leftToRight;
					}
				}
			}
		}

		if (DebugFlag && m_config.DebugMode)
		{
			cv::Mat dispairtyImg;
			CreateDispairtyImage(costScanline, dispairtyImg, m_combinable.local().costValues);
			CImageUtility::dbg_writeImg(dispairtyImg, m_config.DebugPath + "ScanlineOptimization_" + "ButtomToTop_"
				+ direction + "." + m_config.DebugExt);
		}
	}



	template <typename T, int TopencvImgType, typename SideConsistency, bool IsParallel, bool DebugFlag>
	void ScanlineOptimizationTopToButtom(std::vector<cv::Mat>& cost, const cv::Mat& leftRGB, const cv::Mat& rightRGB, std::vector<cv::Mat>& costScanline, std::string direction)
	{
		auto Pai1 = m_config.Pai1, Pai1_quarter = Pai1 / 4.f, Pai1_tenth = Pai1 / 10.f;
		auto Pai2 = m_config.Pai2, Pai2_quarter = Pai2 / 4.f, Pai2_tenth = Pai2 / 10.f;

		auto cols = m_cols, rows = m_rows;

		auto Tau_so = T(m_config.Tau_so);

		for (auto r = 0; r < rows; r++)
		{
			for (auto c = 0; c < cols; c++)
			{
				for (auto d = 0; d < m_config.DisparityRange; d++)
				{
					auto cValidStart = SideConsistency::GetStartColumn(d, cols);
					auto cValidEnd = SideConsistency::GetEndColumn(d, cols);

					auto& costScanlineDisparityD = costScanline[d];

					if (r == 0 || c < cValidStart || c >= cValidEnd)
					{
						costScanlineDisparityD.at<T>(r, c) = cost[d].at<T>(r, c);
					}
					else
					{
						// pd is c location on right image 
						auto pd = SideConsistency::CalcPd(d, c);

						// C1(p,d)
						auto cost_d_r_c = cost[d].at<T>(r, c);

						// Cr(p-r,d)
						auto cost_d_r_cMinusOne = costScanlineDisparityD.at<T>(r - 1, c);

						// Cr(p-r, +-d)
						T cost_dMinusPlusOne_r_cMinusOne;
						if (d == 0)
							cost_dMinusPlusOne_r_cMinusOne = costScanline[d + 1].at<T>(r - 1, c);
						else if (d == (m_config.DisparityRange - 1))
							cost_dMinusPlusOne_r_cMinusOne = costScanline[d - 1].at<T>(r - 1, c);
						else
							cost_dMinusPlusOne_r_cMinusOne = std::min(costScanline[d - 1].at<T>(r - 1, c), costScanline[d + 1].at<T>(r - 1, c));

						// MINkCr(p-r,k)
						T cost_dMin_r_cMinusOne = std::min(cost_dMinusPlusOne_r_cMinusOne, cost_d_r_cMinusOne);
						for (auto dd = 0; dd < m_config.DisparityRange; dd++)
						{
							if (dd == (d - 1) || dd == (d + 1) || dd == d)
							{
								continue;
							}
							else
							{
								cost_dMin_r_cMinusOne = std::min(cost_dMin_r_cMinusOne, costScanline[dd].at<T>(r - 1, c));
							}
						}

						auto left_r_c = leftRGB.at<Vec3b>(r, c);
						auto left_r_cMinusOne = leftRGB.at<Vec3b>(r - 1, c);
						auto right_r_c = rightRGB.at<Vec3b>(r, pd);
						Vec3b right_r_cMinusOne;
						right_r_cMinusOne = rightRGB.at<Vec3b>(r - 1, pd);
						
						T P1, P2;
						// Left to right
						auto D1 = Dc<T>(left_r_c, left_r_cMinusOne);
						auto D2 = Dc<T>(right_r_c, right_r_cMinusOne);
						CalcP1P2<T>(D1, D2, Tau_so, Pai1, Pai2, Pai1_tenth, Pai2_tenth, Pai1_quarter, Pai2_quarter, P1, P2);

						T leftToRight = cost_d_r_c + std::min(cost_d_r_cMinusOne, std::min(cost_dMinusPlusOne_r_cMinusOne + P1, cost_dMin_r_cMinusOne + P2)) - cost_dMin_r_cMinusOne;

						costScanlineDisparityD.at<T>(r, c) = leftToRight;
					}
				}
			}
		}

		if (DebugFlag && m_config.DebugMode)
		{
			cv::Mat dispairtyImg;
			CreateDispairtyImage(costScanline, dispairtyImg, m_combinable.local().costValues);
			CImageUtility::dbg_writeImg(dispairtyImg, m_config.DebugPath + "ScanlineOptimization_" + "TopToButtom_"
				+ direction + "." + m_config.DebugExt);
		}
	}



	template <typename T, int TopencvImgType, typename SideConsistency, bool IsParallel, bool DebugFlag>
	void ScanlineOptimizationLeftToRight(std::vector<cv::Mat>& cost, const cv::Mat& leftRGB, const cv::Mat& rightRGB, std::vector<cv::Mat>& costScanline, std::string direction)
	{
		auto Pai1 = m_config.Pai1, Pai1_quarter = Pai1 / 4.f, Pai1_tenth = Pai1 / 10.f;
		auto Pai2 = m_config.Pai2, Pai2_quarter = Pai2 / 4.f, Pai2_tenth = Pai2 / 10.f;

		auto cols = m_cols, rows = m_rows; 

		auto Tau_so = T(m_config.Tau_so);

		for (auto r = 0; r < rows; r++)
		{
			for (auto c = 0; c < cols; c++)
			{
					for (auto d = 0; d < m_config.DisparityRange; d++)
					{
						auto cValidStart = SideConsistency::GetStartColumn(d, cols);
						auto cValidEnd = SideConsistency::GetEndColumn(d, cols);

						auto& costScanlineDisparityD = costScanline[d];

						if (c == 0 || c < cValidStart || c >= cValidEnd)
						{
							costScanlineDisparityD.at<T>(r, c) = cost[d].at<T>(r, c);
						}
						else
						{
							// pd is c location on right image 
							auto pd = SideConsistency::CalcPd(d, c);

							// C1(p,d)
							auto cost_d_r_c = cost[d].at<T>(r, c);

							// Cr(p-r,d)
							auto cost_d_r_cMinusOne = costScanlineDisparityD.at<T>(r, c - 1);

							// Cr(p-r, +-d)
							T cost_dMinusPlusOne_r_cMinusOne;
							if (d == 0)
								cost_dMinusPlusOne_r_cMinusOne = costScanline[d + 1].at<T>(r, c - 1);
							else if (d == (m_config.DisparityRange - 1))
								cost_dMinusPlusOne_r_cMinusOne = costScanline[d - 1].at<T>(r, c - 1);
							else 
								cost_dMinusPlusOne_r_cMinusOne = std::min(costScanline[d - 1].at<T>(r, c - 1), costScanline[d + 1].at<T>(r, c - 1));

							// MINkCr(p-r,k)
							T cost_dMin_r_cMinusOne = std::min(cost_dMinusPlusOne_r_cMinusOne, cost_d_r_cMinusOne);
							for (auto dd = 0; dd < m_config.DisparityRange; dd++)
							{
								if (dd == (d - 1) || dd == (d + 1) || dd == d)
								{
									continue;
								}
								else
								{
									cost_dMin_r_cMinusOne = std::min(cost_dMin_r_cMinusOne, costScanline[dd].at<T>(r, c - 1));
								}
							}

							auto left_r_c = leftRGB.at<Vec3b>(r, c);
							auto left_r_cMinusOne = leftRGB.at<Vec3b>(r, c - 1);
							auto right_r_c = rightRGB.at<Vec3b>(r, pd);
							Vec3b right_r_cMinusOne;
							if (pd > 0)
								right_r_cMinusOne = rightRGB.at<Vec3b>(r, pd - 1);
							else
								right_r_cMinusOne = rightRGB.at<Vec3b>(r, pd + 1);

							T P1, P2;
							// Left to right
							auto D1 = Dc<T>(left_r_c, left_r_cMinusOne);
							auto D2 = Dc<T>(right_r_c, right_r_cMinusOne);
							CalcP1P2<T>(D1, D2, Tau_so, Pai1, Pai2, Pai1_tenth, Pai2_tenth, Pai1_quarter, Pai2_quarter, P1, P2);

							T leftToRight = cost_d_r_c + std::min(cost_d_r_cMinusOne, std::min(cost_dMinusPlusOne_r_cMinusOne + P1, cost_dMin_r_cMinusOne + P2)) - cost_dMin_r_cMinusOne;

							costScanlineDisparityD.at<T>(r, c) = leftToRight;
						}
					}
				}
			}

			if (DebugFlag && m_config.DebugMode)
			{
				cv::Mat dispairtyImg;
				CreateDispairtyImage(costScanline, dispairtyImg, m_combinable.local().costValues);
				CImageUtility::dbg_writeImg(dispairtyImg, m_config.DebugPath + "ScanlineOptimization_" + "LeftToRight_" 
					+ direction +"." + m_config.DebugExt);
			}

		}


	template <typename T, int TopencvImgType, typename SideConsistency, bool IsParallel, bool DebugFlag>
	void ScanlineOptimizationRightToLeft(std::vector<cv::Mat>& cost, const cv::Mat& leftRGB, const cv::Mat& rightRGB, std::vector<cv::Mat>& costScanline, std::string direction)
	{
		auto Pai1 = m_config.Pai1, Pai1_quarter = Pai1 / 4.f, Pai1_tenth = Pai1 / 10.f;
		auto Pai2 = m_config.Pai2, Pai2_quarter = Pai2 / 4.f, Pai2_tenth = Pai2 / 10.f;

		auto cols = m_cols, rows = m_rows;

		auto Tau_so = T(m_config.Tau_so);

		for (auto r = 0; r < rows; r++)
		{
			for (auto c = cols - 1; c >= 0; c--)
			{
				for (auto d = 0; d < m_config.DisparityRange; d++)
				{
					auto cValidStart = SideConsistency::GetStartColumn(d, cols);
					auto cValidEnd = SideConsistency::GetEndColumn(d, cols);

					auto& costScanlineDisparityD = costScanline[d];

					if (c == 0 || c < cValidStart || c >= (cValidEnd - 1))
					{
						costScanlineDisparityD.at<T>(r, c) = cost[d].at<T>(r, c);
					}
					else
					{
						// pd is c location on right image 
						auto pd = SideConsistency::CalcPd(d, c);

						// C1(p,d)
						auto cost_d_r_c = cost[d].at<T>(r, c);

						// Cr(p-r,d)
						auto cost_d_r_cMinusOne = costScanlineDisparityD.at<T>(r, c + 1);

						// Cr(p-r, +-d)
						T cost_dMinusPlusOne_r_cMinusOne;
						if (d == 0)
							cost_dMinusPlusOne_r_cMinusOne = costScanline[d + 1].at<T>(r, c + 1);
						else if (d == (m_config.DisparityRange - 1))
							cost_dMinusPlusOne_r_cMinusOne = costScanline[d - 1].at<T>(r, c + 1);
						else
							cost_dMinusPlusOne_r_cMinusOne = std::min(costScanline[d - 1].at<T>(r, c + 1), costScanline[d + 1].at<T>(r, c + 1));

						// MINkCr(p-r,k)
						T cost_dMin_r_cMinusOne = std::min(cost_dMinusPlusOne_r_cMinusOne, cost_d_r_cMinusOne);
						for (auto dd = 0; dd < m_config.DisparityRange; dd++)
						{
							if (dd == (d - 1) || dd == (d + 1) || dd == d)
							{
								continue;
							}
							else
							{
								cost_dMin_r_cMinusOne = std::min(cost_dMin_r_cMinusOne, costScanline[dd].at<T>(r, c + 1));
							}
						}

						auto left_r_c = leftRGB.at<Vec3b>(r, c);
						auto left_r_cMinusOne = leftRGB.at<Vec3b>(r, c + 1);
						auto right_r_c = rightRGB.at<Vec3b>(r, pd);
						Vec3b right_r_cMinusOne;
						if (pd >= -1)
							right_r_cMinusOne = rightRGB.at<Vec3b>(r, pd + 1);
						else
							right_r_cMinusOne = rightRGB.at<Vec3b>(r, pd - 1);

						T P1, P2;
						// Left to right
						auto D1 = Dc<T>(left_r_c, left_r_cMinusOne);
						auto D2 = Dc<T>(right_r_c, right_r_cMinusOne);
						CalcP1P2<T>(D1, D2, Tau_so, Pai1, Pai2, Pai1_tenth, Pai2_tenth, Pai1_quarter, Pai2_quarter, P1, P2);

						T leftToRight = cost_d_r_c + std::min(cost_d_r_cMinusOne, std::min(cost_dMinusPlusOne_r_cMinusOne + P1, cost_dMin_r_cMinusOne + P2)) - cost_dMin_r_cMinusOne;

						costScanlineDisparityD.at<T>(r, c) = leftToRight;
					}
				}
			}
		}

		if (DebugFlag && m_config.DebugMode)
		{
			cv::Mat dispairtyImg;
			CreateDispairtyImage(costScanline, dispairtyImg, m_combinable.local().costValues);
			CImageUtility::dbg_writeImg(dispairtyImg, m_config.DebugPath + "ScanlineOptimization_" + "RightToLeft_"
				+ direction + "." + m_config.DebugExt);
		}

	}


	template <typename T, int TopencvImgType, typename SideConsistency>
	void ScanlineOptimization(std::vector<cv::Mat>& cost, const cv::Mat& minCost,
		const cv::Mat& leftRGB, const cv::Mat& rightRGB, ThreadLocalData& threadLocalData, int rows, int cols, int d)
	{
		// Old code 
		if (false)
		{
			// Update the cost in place, run the four scan lines directions at one function 
			// Exclude one pixel at each dimension because the scan line works on previous pixel on scanline direction
			auto cValidStart = SideConsistency::GetStartColumn(d, cols);
			auto cValidEnd = SideConsistency::GetEndColumn(d, cols);

			auto Pai1 = m_config.Pai1, Pai1_quarter = Pai1 / 4.f, Pai1_tenth = Pai1 / 10.f;
			auto Pai2 = m_config.Pai2, Pai2_quarter = Pai2 / 4.f, Pai2_tenth = Pai2 / 10.f;

			auto Tau_so = T(m_config.Tau_so);

			auto rowsMinusOne = rows - 1;

			// Calculate min matrix for +- d values 

			Mat minCostDispairtyMinusPlusOne;
			if (d > 0 && d < (m_config.DisparityRange - 1))
			{
				minCostDispairtyMinusPlusOne = cv::min(cost[d - 1], cost[d + 1]);
			}
			else
			{
				if (d == 0)
				{
					minCostDispairtyMinusPlusOne = cost[1];
				}
				else // d == (m_config.DisparityRange - 1)
				{
					minCostDispairtyMinusPlusOne = cost[m_config.DisparityRange - 2];
				}
			}

			// Clone the cost, because we'd like to update the cost in place, the scan line directions needs the origianl buffer, in case we update the result in place, we'd better clone the buffer 
			auto& costImg = cost[d];
			Mat& costCopy = threadLocalData.auxImg = costImg.clone();

			//to do, execute ScanlineOptimization for each direction individually



			// Exclude one pixel at each direction 
			for (auto r = 1; r < rowsMinusOne; r++)
			{
				for (auto c = cValidStart + 1; c < cValidEnd - 1; c++)
				{
					// pd is c location on right image 
					auto pd = SideConsistency::CalcPd(d, c);

					auto cost_d_r_c = costCopy.at<T>(r, c);

					auto cost_d_r_cMinusOne = costCopy.at<T>(r, c - 1);
					auto cost_d_r_cPlusOne = costCopy.at<T>(r, c + 1);
					auto cost_d_rMinusOne_c = costCopy.at<T>(r - 1, c);
					auto cost_d_rPlusOne_c = costCopy.at<T>(r + 1, c);

					auto cost_dMinusPlusOne_r_cMinusOne = minCostDispairtyMinusPlusOne.at<T>(r, c - 1);
					auto cost_dMinusPlusOne_r_cPlusOne = minCostDispairtyMinusPlusOne.at<T>(r, c + 1);
					auto cost_dMinusPlusOne_rMinusOne_c = minCostDispairtyMinusPlusOne.at<T>(r - 1, c);
					auto cost_dMinusPlusOne_rPlusOne_c = minCostDispairtyMinusPlusOne.at<T>(r + 1, c);

					auto cost_dMin_r_cMinusOne = minCost.at<T>(r, c - 1);
					auto cost_dMin_r_cPlusOne = minCost.at<T>(r, c + 1);
					auto cost_dMin_rMinusOne_c = minCost.at<T>(r - 1, c);
					auto cost_dMin_rPlusOne_c = minCost.at<T>(r + 1, c);

					auto left_r_c = leftRGB.at<Vec3b>(r, c);
					auto left_r_cMinusOne = leftRGB.at<Vec3b>(r, c - 1);
					auto left_r_cPlusOne = leftRGB.at<Vec3b>(r, c + 1);
					auto left_rMinusOne_c = leftRGB.at<Vec3b>(r - 1, c);
					auto left_rPlusOne_c = leftRGB.at<Vec3b>(r + 1, c);

					auto right_r_c = rightRGB.at<Vec3b>(r, pd);
					auto right_r_cMinusOne = rightRGB.at<Vec3b>(r, pd - 1);
					auto right_r_cPlusOne = rightRGB.at<Vec3b>(r, pd + 1);
					auto right_rMinusOne_c = rightRGB.at<Vec3b>(r - 1, pd);
					auto right_rPlusOne_c = rightRGB.at<Vec3b>(r + 1, pd);

					T P1, P2;
					// Left to right 
					CalcP1P2<T>(Dc<T>(left_r_c, left_r_cMinusOne), Dc<T>(right_r_c, right_r_cMinusOne), Tau_so, Pai1, Pai2, Pai1_tenth, Pai2_tenth, Pai1_quarter, Pai2_quarter, P1, P2);
					T leftToRight = cost_d_r_c + std::min(cost_d_r_cMinusOne, std::min(cost_dMinusPlusOne_r_cMinusOne + P1, cost_dMin_r_cMinusOne + P2)) - cost_dMin_r_cMinusOne;

					// Right to left 
					CalcP1P2<T>(Dc<T>(left_r_c, left_r_cPlusOne), Dc<T>(right_r_c, right_r_cPlusOne), Tau_so, Pai1, Pai2, Pai1_tenth, Pai2_tenth, Pai1_quarter, Pai2_quarter, P1, P2);
					T rightToLeft = cost_d_r_c + std::min(cost_d_r_cPlusOne, std::min(cost_dMinusPlusOne_r_cPlusOne + P1, cost_dMin_r_cPlusOne + P2)) - cost_dMin_r_cPlusOne;

					// Top to bottom
					CalcP1P2<T>(Dc<T>(left_r_c, left_rMinusOne_c), Dc<T>(right_r_c, right_rMinusOne_c), Tau_so, Pai1, Pai2, Pai1_tenth, Pai2_tenth, Pai1_quarter, Pai2_quarter, P1, P2);
					T topToBottom = cost_d_r_c + std::min(cost_d_rMinusOne_c, std::min(cost_dMinusPlusOne_rMinusOne_c + P1, cost_dMin_rMinusOne_c + P2)) - cost_dMin_rMinusOne_c;

					// Bottom to top 
					CalcP1P2<T>(Dc<T>(left_r_c, left_rPlusOne_c), Dc<T>(right_r_c, right_rPlusOne_c), Tau_so, Pai1, Pai2, Pai1_tenth, Pai2_tenth, Pai1_quarter, Pai2_quarter, P1, P2);
					T bottomToTop = cost_d_r_c + std::min(cost_d_rPlusOne_c, std::min(cost_dMinusPlusOne_rPlusOne_c + P1, cost_dMin_rPlusOne_c + P2)) - cost_dMin_rPlusOne_c;

					costImg.at<T>(r, c) = (leftToRight + rightToLeft + topToBottom + bottomToTop) / 4.f;
				}
			}
		}
		else
		{
			// Update the cost in place
			// Exclude one pixel at each dimension because the scan line works on previous pixel on scanline direction
			auto cValidStart = SideConsistency::GetStartColumn(d, cols);
			auto cValidEnd = SideConsistency::GetEndColumn(d, cols);

			auto Pai1 = m_config.Pai1, Pai1_quarter = Pai1 / 4.f, Pai1_tenth = Pai1 / 10.f;
			auto Pai2 = m_config.Pai2, Pai2_quarter = Pai2 / 4.f, Pai2_tenth = Pai2 / 10.f;

			auto Tau_so = T(m_config.Tau_so);

			// auto rowsMinusOne = rows - 1;

			// Calculate min matrix for +- d values 
			Mat minCostDispairtyMinusPlusOne;
			if (d > 0 && d < (m_config.DisparityRange - 1))
			{
				minCostDispairtyMinusPlusOne = cv::min(cost[d - 1], cost[d + 1]);
			}
			else
			{
				if (d == 0)
				{
					minCostDispairtyMinusPlusOne = cost[1];
				}
				else // d == (m_config.DisparityRange - 1)
				{
					minCostDispairtyMinusPlusOne = cost[m_config.DisparityRange - 2];
				}
			}

			// Clone the cost, because we'd like to update the cost in place, the scan line directions needs the original buffer, 
			// in case we update the result in place, we'd better clone the buffer 
			auto& costImg = cost[d];
			Mat& costScanlineDirection = threadLocalData.auxImg = costImg.clone();

			//to do, execute ScanlineOptimization for each direction individually

			// Left to right 
			for (auto r = 1; r < rows - 1; r++)
			{
				for (auto c = cValidStart + 1; c < cValidEnd - 1; c++)
				{
					// pd is c location on right image 
					auto pd = SideConsistency::CalcPd(d, c);

					// C1(p,d)
					auto cost_d_r_c = costImg.at<T>(r, c);

					// Cr(p-r,d)
					auto cost_d_r_cMinusOne = costScanlineDirection.at<T>(r, c - 1);

					// Cr(p-r, d)
					auto cost_dMinusPlusOne_r_cMinusOne = minCostDispairtyMinusPlusOne.at<T>(r, c - 1);

					// MINkCr(p-r,k)
					auto cost_dMin_r_cMinusOne = minCost.at<T>(r, c - 1);

					auto left_r_c = leftRGB.at<Vec3b>(r, c);
					auto left_r_cMinusOne = leftRGB.at<Vec3b>(r, c - 1);
					auto right_r_c = rightRGB.at<Vec3b>(r, pd);
					auto right_r_cMinusOne = rightRGB.at<Vec3b>(r, pd - 1);
					T P1, P2;
					// Left to right
					auto D1 = Dc<T>(left_r_c, left_r_cMinusOne);
					auto D2 = Dc<T>(right_r_c, right_r_cMinusOne);
					CalcP1P2<T>(D1, D2, Tau_so, Pai1, Pai2, Pai1_tenth, Pai2_tenth, Pai1_quarter, Pai2_quarter, P1, P2);
					T leftToRight = cost_d_r_c + std::min(cost_d_r_cMinusOne, std::min(cost_dMinusPlusOne_r_cMinusOne + P1, cost_dMin_r_cMinusOne + P2)) - cost_dMin_r_cMinusOne;

					costScanlineDirection.at<T>(r, c) = leftToRight;
				}
			}
			cost[d] = costScanlineDirection;
		}
	}


	template <typename T>
	inline void CalcP1P2(T D1, T D2, T Tau_so, T Pai1, T Pai2, T Pai1_tenth, T Pai2_tenth, T Pai1_quarter, T Pai2_quarter, T& P1, T& P2)
	{
		if (D1 < Tau_so && D2 < Tau_so)
		{
			P1 = Pai1;
			P2 = Pai2;
		}
		else if (D1 > Tau_so && D2 > Tau_so)
		{
			P1 = P2 = Pai1_tenth;
		}
		else
		{
			P1 = Pai1_quarter;
			P2 = Pai2_quarter;
		}
	}


	///////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////
	// Create disparity image from cost matrix 
	void CreateDispairtyImage(std::vector<cv::Mat>& cost, cv::Mat& dispairtyImg, std::vector<float>& costValues);

	///////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////
	// Outlier Detections functions 
	void OutlierDetections();

	void IterativeRegionVoting();

	void SubPixelEnhancement();

	///////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////
	// Management functions 
	template <int cw, int ch>
	void ComputeStereoMatching(const cv::Mat& left, const cv::Mat& right, cv::Mat& dispairyMap);

	template <int ch>
	void ComputeStereoMatchingCW(const cv::Mat& left, const cv::Mat& right, cv::Mat& dispairyMapL2R);

	template <bool IsTwoDirections, typename T, int percisionTypeOpencvImg>
	void Allocate(const cv::Mat& left, const cv::Mat& right, cv::Mat& dispairyMapL2R);

	template <typename T, int percisionTypeOpencvImg>
	void InitializCostMultiMatrix(std::vector<cv::Mat>& cost)
	{
		cost.resize(m_config.DisparityRange);
		for (auto i = 0; i < m_config.DisparityRange; i++)
		{
			cost[i].create(m_rows, m_cols, percisionTypeOpencvImg);
			cost[i].setTo(GetMaxCostValue<T>());
		}
	}

	template <typename T, int percisionTypeOpencvImg>
	void InitializeScanlineOptimazation(std::vector<cv::Mat> cost[4])
	{
		for (auto qq = 0; qq < 4; qq++)
		{
			auto& c = cost[qq];
			c.resize(m_config.DisparityRange);
			for (auto& i : c)
			{
				i.create(m_rows, m_cols, percisionTypeOpencvImg);
			}
		}
	}


	void InitializeAggregationRegion(AggregatedRegion& aggregatedRegions);

	void DeAllocate();

	// Convert RGB to gray, create pyramids and create census image 
	template <bool IsTwoDirections, typename T, bool IsParallel, bool DebugFlag>
	void PreprocessOperations();

	template <typename T, bool IsCalcAggregatedRegions, bool IsParallel, bool DebugFlag>
	void PreprocessOperations(cv::Mat& rgb_org, cv::Mat& rgb, cv::Mat& gray, std::vector<std::bitset<cw * ch>>& census, AggregatedRegion& aggregatedRegions);

	template <bool IsTwoDirections, typename percisionType, int percisionTypeOpencvImg, bool IsParallel, bool DebugFlag>
	void ApplyBiDirections();

	template <bool IsTwoDirections, typename percisionType, int percisionTypeOpencvImg, bool IsParallel, bool DebugFlag>
	void ApplyL2R();

	template <bool IsTwoDirections, typename percisionType, int percisionTypeOpencvImg, bool IsParallel, bool DebugFlag>
	void ApplyR2L();

	// Create input image pyramid 
	void CreatePyramids(cv::Mat& rgb_org, cv::Mat& img);

	void ConvertRGBFromMatlabToOpencv(const uchar* ptr, int rows, int cols, cv::Mat& dstImg);

	void SetResultToUsersData();

private:
	// Images dims on working pyramid level
	int m_rows, m_cols;

	// Original image given from user
	cv::Mat m_imgLeftRGB_org, m_imgRightRGB_org, m_dispairyImgL2R_org;

	// Used for ComputeStereoMatchingMatlab function 
	cv::Mat m_imgLeftTranspose, m_imgRightTranspose, m_disparityMapTranspose;

	// Down sampled image 
	cv::Mat m_imgLeftRGB, m_imgRightRGB, m_imgLeftGrayscale, m_imgRightGrayscale;

	cv::Mat m_dispairityImgL2R, m_dispairityImgR2L;

	// Cost function, L2R - LeftToRight
	std::vector<cv::Mat> m_costL2R, m_costR2L;

	// Hold a Census bitwise representation of an image 
	std::vector<std::bitset<cw * ch>> m_censusImgLeft, m_censusImgRight;

	// Hold the aggregated regions for each image, First vector used to represent each pixel in the image, 2nd vector holds a the start and end points for each region. 
	AggregatedRegion m_aggregatedRegionsLeft, m_aggregatedRegionsRight;

	std::vector<cv::Mat> m_costScanlineLeft[4], m_costScanlineRight[4];

	// Used in parallel version 
	concurrency::combinable<ThreadLocalData> m_combinable;

	// For multi direction mode 
	cv::Mat m_outlierMap;
	cv::Mat m_histogramMat;
	/*constexpr */const uchar valid = 0;
	/*constexpr */const uchar mismach = 255;

	// Sub pixel enhancement data 
	cv::Mat m_numerator, m_dTemp1, m_dDominator;

	StereoMatchingConfig m_config;
};




template <int cw, int ch>
StereoMatching<cw, ch>::StereoMatching(const StereoMatchingConfig& config) : m_config(config)
{
}


template <int cw, int ch>
StereoMatching<cw, ch>::~StereoMatching()
{
}


template <int cw, int ch>
void StereoMatching<cw, ch>::ComputeStereoMatching(const Mat& left, const Mat& right, Mat& dispairyMapL2R)
{
	const bool IsParallel = true, DebugFlag = false, IsTwoDirections = true;

	typedef float percisionType;
	constexpr int percisionTypeOpencvImg = CV_32FC1;

	Allocate<IsTwoDirections, percisionType, percisionTypeOpencvImg>(left, right, dispairyMapL2R);

	PreprocessOperations<IsTwoDirections, percisionType, IsParallel, DebugFlag>();

	ApplyBiDirections<IsTwoDirections, percisionType, percisionTypeOpencvImg, IsParallel, DebugFlag>();

	if (m_config.isApplyDisparityRefinement)
	{
		if (IsTwoDirections)
		{
			OutlierDetections();

			IterativeRegionVoting();

			// Shahar Sar Shalom TODO:  proper interpolation as mentioned in the article 

			// Shahar Sar Shalom TODO:  Depth discontinuity adjustment as mentioned in the article 
		}

		SubPixelEnhancement();

		if (false)
			medianBlur(m_dispairityImgL2R, m_dispairityImgL2R, 3);

	}
	SetResultToUsersData();

	if (DebugFlag && m_config.DebugMode)
	{
		CImageUtility::dbg_writeImg(dispairyMapL2R, m_config.DebugPath + "ComputeStereoMatching" + "." + m_config.DebugExt);
	}
}


template <int cw, int ch>
void StereoMatching<cw, ch>::ComputeStereoMatchingMatlab(const uchar* leftPtr, const uchar* rightPtr, float* disparityMapPtr, int rows, int cols)
{
	ConvertRGBFromMatlabToOpencv(leftPtr, rows, cols, m_imgLeftTranspose);

	ConvertRGBFromMatlabToOpencv(rightPtr, rows, cols, m_imgRightTranspose);

	ComputeStereoMatching(m_imgLeftTranspose, m_imgRightTranspose, m_disparityMapTranspose);

	Mat disparity(cols, rows, CV_32FC1, (void*)disparityMapPtr);
	transpose(m_disparityMapTranspose, disparity);
}


template <int cw, int ch>
void StereoMatching<cw, ch>::ConvertRGBFromMatlabToOpencv(const uchar* ptr, int rows, int cols, cv::Mat& dstImg)
{
	Mat imgTranspose;
	Mat img(cols * 3, rows, CV_8UC1, (void*)ptr);

	transpose(img, imgTranspose);

	vector<Mat> listMat;
	//for (auto i = 2; i > 0; i--)
	for (auto i = 0; i < 3; i++)
	{
		auto m = Mat(rows, cols, CV_8UC1, imgTranspose.data + cols * i, imgTranspose.step);
		listMat.push_back(m);
	}

	merge(listMat, dstImg);

	if (false)
	{
		CImageUtility::dbg_writeImg(dstImg, m_config.DebugPath + "ConvertRGBFromMatlabToOpencv" + "." + m_config.DebugExt);
	}
}


template <int cw, int ch>
template <bool IsTwoDirections, typename percisionType, int percisionTypeOpencvImg, bool IsParallel, bool DebugFlag>
void StereoMatching<cw, ch>::ApplyBiDirections()
{
	// Calculate AD census image m_stereoMatchingConfig
	if (IsTwoDirections)
	{
		if (IsParallel)
		{
			task_group task;
			task.run([this]() {
				ApplyR2L<IsTwoDirections, percisionType, percisionTypeOpencvImg, IsParallel, DebugFlag>();
			});
			ApplyL2R<IsTwoDirections, percisionType, percisionTypeOpencvImg, IsParallel, DebugFlag>();
			task.wait();
		}
		else
		{
			ApplyR2L<IsTwoDirections, percisionType, percisionTypeOpencvImg, IsParallel, DebugFlag>();
			ApplyL2R<IsTwoDirections, percisionType, percisionTypeOpencvImg, IsParallel, DebugFlag>();
		}
	}
	else
	{
		ApplyL2R<IsTwoDirections, percisionType, percisionTypeOpencvImg, IsParallel, DebugFlag>();
	}
}


template <int cw, int ch>
template <bool IsTwoDirections, typename percisionType, int percisionTypeOpencvImg, bool IsParallel, bool DebugFlag>
void StereoMatching<cw, ch>::ApplyR2L()
{
	ADCensusCostInitialization<percisionType, RightToLeftConsistency, IsParallel, DebugFlag>(m_imgRightRGB, m_imgRightGrayscale, m_imgLeftRGB, m_imgLeftGrayscale, m_costR2L, "R2L");

	if (m_config.isApplyCostAggregation)
		CrossBasedCostAggregation<percisionType, percisionTypeOpencvImg, RightToLeftConsistency, IsParallel, DebugFlag>(m_aggregatedRegionsRight, m_costR2L, "R2L");

	if (m_config.isApplyScanlineOptimization)
		ScanlineOptimization<percisionType, percisionTypeOpencvImg, RightToLeftConsistency, IsParallel, DebugFlag>(m_costR2L, m_imgRightRGB, m_imgLeftRGB, m_costScanlineRight, "R2L");

	CreateDispairtyImage(m_costR2L, m_dispairityImgR2L, m_combinable.local().costValues);
}


template <int cw, int ch>
template <bool IsTwoDirections, typename percisionType, int percisionTypeOpencvImg, bool IsParallel, bool DebugFlag>
void StereoMatching<cw, ch>::ApplyL2R()
{
	ADCensusCostInitialization<percisionType, LeftToRightConsistency, IsParallel, DebugFlag>(m_imgLeftRGB, m_imgLeftGrayscale, m_imgRightRGB, m_imgRightGrayscale, m_costL2R, "L2R");

	if (m_config.isApplyCostAggregation)
		CrossBasedCostAggregation<percisionType, percisionTypeOpencvImg, LeftToRightConsistency, IsParallel, DebugFlag>(m_aggregatedRegionsLeft, m_costL2R, "L2R");

	if (m_config.isApplyScanlineOptimization)
		ScanlineOptimization<percisionType, percisionTypeOpencvImg, LeftToRightConsistency, IsParallel, DebugFlag>(m_costL2R, m_imgLeftRGB, m_imgRightRGB, m_costScanlineLeft, "L2R");

	CreateDispairtyImage(m_costL2R, m_dispairityImgL2R, m_combinable.local().costValues);
}


template <int cw, int ch>
template <bool IsTwoDirections, typename T, int percisionTypeOpencvImg>
void StereoMatching<cw, ch>::Allocate(const Mat& left, const Mat& right, Mat& dispairyMapL2R)
{
	m_imgLeftRGB_org = left;
	m_imgRightRGB_org = right;
	dispairyMapL2R.create(left.rows, left.cols, CV_32FC1);
	m_dispairyImgL2R_org = dispairyMapL2R;

	// expected size after pyramids 
	auto pyr_rows = left.rows, pyr_cols = left.cols;

	// Calculate expected pyramid size
	for (auto i = 0; i < m_config.NumPyramids; i++)
	{
		pyr_rows = (pyr_rows + 1) / 2;
		pyr_cols = (pyr_cols + 1) / 2;
	}

	if (m_rows != pyr_rows || m_cols != pyr_cols)
	{
		m_rows = pyr_rows;
		m_cols = pyr_cols;

		auto size = m_rows * m_cols;

		m_censusImgLeft.resize(size);
		m_censusImgRight.resize(size);

		// Create the cost matrix
		InitializCostMultiMatrix<T, percisionTypeOpencvImg>(m_costL2R);
		InitializeAggregationRegion(m_aggregatedRegionsLeft);
		InitializeScanlineOptimazation<T, percisionTypeOpencvImg>(m_costScanlineLeft);

		if (IsTwoDirections)
		{
			InitializCostMultiMatrix<T, percisionTypeOpencvImg>(m_costR2L);
			InitializeAggregationRegion(m_aggregatedRegionsRight);
			InitializeScanlineOptimazation<T, percisionTypeOpencvImg>(m_costScanlineRight);
		}
	}
}


template <int cw, int ch>
void StereoMatching<cw, ch>::InitializeAggregationRegion(AggregatedRegion& aggregatedRegions)
{
	auto maxPixelDistance = m_config.L1;

	aggregatedRegions.resize(m_rows * m_cols);
	for (auto& i : aggregatedRegions)
	{
		i.reserve(maxPixelDistance);
	}
}


template <int cw, int ch>
void StereoMatching<cw, ch>::DeAllocate()
{
}


template <int cw, int ch>
template <bool IsTwoDirections, typename T, bool IsParallel, bool DebugFlag>
void StereoMatching<cw, ch>::PreprocessOperations()
{
	if (IsParallel)
	{
		task_group task;
		task.run([this]() {
			PreprocessOperations<T, true, IsParallel, DebugFlag>(m_imgLeftRGB_org, m_imgLeftRGB, m_imgLeftGrayscale, m_censusImgLeft, m_aggregatedRegionsLeft);
		});

		PreprocessOperations<T, IsTwoDirections, IsParallel, DebugFlag>(m_imgRightRGB_org, m_imgRightRGB, m_imgRightGrayscale, m_censusImgRight, m_aggregatedRegionsRight);

		task.wait();
	}
	else
	{
		PreprocessOperations<T, true, IsParallel, DebugFlag>(m_imgLeftRGB_org, m_imgLeftRGB, m_imgLeftGrayscale, m_censusImgLeft, m_aggregatedRegionsLeft);
		PreprocessOperations<T, IsTwoDirections, IsParallel, DebugFlag>(m_imgRightRGB_org, m_imgRightRGB, m_imgRightGrayscale, m_censusImgRight, m_aggregatedRegionsRight);
	}
}


template <int cw, int ch>
template <typename T, bool IsCalcAggregatedRegions, bool IsParallel, bool DebugFlag>
void StereoMatching<cw, ch>::PreprocessOperations(cv::Mat& rgb_org, cv::Mat& rgb, cv::Mat& gray, std::vector<std::bitset<cw * ch>>& census, vector<vector<tuple<cv::Point2i, cv::Point2i>>>& aggregatedRegions)
{
	CreatePyramids(rgb_org, rgb);

	// ConvertToGray
	cvtColor(rgb, gray, CV_BGR2GRAY);

	// Create census image 
	CreateCensus<IsParallel>(gray, census);

	if (IsCalcAggregatedRegions)
	{
		CalcAggregatedRegions<T, IsParallel, DebugFlag>(rgb, aggregatedRegions);
	}
}


template <int cw, int ch>
void StereoMatching<cw, ch>::CreatePyramids(cv::Mat& rgb_org, cv::Mat& rgb)
{
	if (m_config.NumPyramids)
	{
		auto& src = rgb_org;
		for (auto i = 0; i < m_config.NumPyramids; i++)
		{
			if (i != 0)
				src = rgb;

			pyrDown(src, rgb);
		}
	}
	else
	{
		rgb = rgb_org;
	}

	assert(m_rows == rgb.rows && m_cols == rgb.cols);
}


/////////////////////////////////////////////////////////////// AD census functions


template <int cw, int ch>
template <bool IsParallel>
void StereoMatching<cw, ch>::CreateCensus(const Mat& srcGray, std::vector<std::bitset<cw * ch>>& censusImg)
{
	auto rows = m_rows;

	if (IsParallel)
	{
		parallel_for(int(0), rows, [&](int r)
		{
			CreateCensus(srcGray, censusImg, r);
		});
	}
	else
	{
		for (auto r = 0; r < rows; r++)
		{
			CreateCensus(srcGray, censusImg, r);
		}
	}
}


template <int cw, int ch>
void StereoMatching<cw, ch>::CreateCensus(const Mat& srcGray, std::vector<std::bitset<cw * ch>>& censusImg, int r)
{
	Rect roiPadded;
	roiPadded.width = cw;
	roiPadded.height = ch;

	constexpr auto NumBits = cw * ch;
	constexpr auto cwHalfDim = cw / 2;
	constexpr auto chHalfDim = ch / 2;

	std::bitset<NumBits> bitwise;
	auto cols = m_cols;
	auto rows = m_rows;

	// form a border in-place
	cv::Mat srcGrayPadded;
	copyMakeBorder(srcGray, srcGrayPadded, chHalfDim, chHalfDim, cwHalfDim, cwHalfDim, BORDER_REFLECT);

	uchar censusWindow[NumBits];
	Mat censusWindowMat(ch, cw, CV_8UC1, &censusWindow[0]);

	for (auto c = 0; c < cols; c++)
	{
		// Calculate census cost

		// Census pixel rounding, Left census ROI location, note that roi in paded image scope 
		roiPadded.x = c;
		roiPadded.y = r;
		{
			auto srcRoi = srcGrayPadded(roiPadded);
			uchar rightMiddleValue = srcRoi.at<uchar>(cwHalfDim, chHalfDim);

			compare(srcRoi, rightMiddleValue, censusWindowMat, CmpTypes::CMP_GT);

			// Consider using pragma unroll 
			for (auto i = 0; i < NumBits; i++)
			{
				bitwise.set(i, censusWindow[i] == 0);
			}

			censusImg[r * cols + c] = bitwise;
		}
	}
}


template <int cw, int ch>
template <typename T, typename SideConsistency, bool IsParallel, bool DebugFlag>
void StereoMatching<cw, ch>::ADCensusCostInitialization(const Mat& leftRGB, const Mat& leftGray, const Mat& rightRGB, const Mat& rightGray, vector<Mat>& cost, std::string direction)
{
	if (IsParallel)
	{
		parallel_for(int(0), m_config.DisparityRange, [&](int d)
		{
			ADCensusCostInitialization<T, SideConsistency>(leftRGB, leftGray, rightRGB, rightGray, cost, d);
		});
	}
	else
	{
		for (auto d = 0; d < m_config.DisparityRange; d++)
		{
			ADCensusCostInitialization<T, SideConsistency>(leftRGB, leftGray, rightRGB, rightGray, cost, d);
		}
	}

	if (DebugFlag && m_config.DebugMode)
	{
		cv::Mat dispairtyImg;

		CreateDispairtyImage(cost, dispairtyImg, m_combinable.local().costValues);
		CImageUtility::dbg_writeImg(dispairtyImg, m_config.DebugPath + "ADCensusCostInitialization" + direction + "." + m_config.DebugExt);
	}
}


template <int cw, int ch>
template <typename T, typename SideConsistency>
void StereoMatching<cw, ch>::ADCensusCostInitialization(const Mat& leftRGB, const Mat& leftGray, const Mat& rightRGB, const Mat& rightGray, vector<Mat>& cost, int d)
{
	constexpr auto NumBits = cw * ch;

	auto rows = m_rows;
	auto cols = m_cols;

	constexpr auto cwHalfDim = cw / 2;
	constexpr auto chHalfDim = ch / 2;

	// Handled the census roi region box 
	auto rowsLimit = rows - 2 * chHalfDim;
	auto colsLimit = cols - 2 * cwHalfDim;

	auto& censusImgLeft = m_censusImgLeft;
	auto& censusImgRight = m_censusImgRight;


	Rect leftRoi;
	leftRoi.width = cw;
	leftRoi.height = ch;

	Rect rightRoi;
	rightRoi.width = cw;
	rightRoi.height = ch;

	auto& dispairtyCostL2R = cost[d];

	for (auto r = 0; r < rows; r++)
	{
		// AD census only the valid regions of the cost function side
		auto cValidStart = SideConsistency::GetStartColumn(d, cols);
		auto cValidEnd = SideConsistency::GetEndColumn(d, cols);

		for (auto c = cValidStart; c < cValidEnd; c++)
		{
			auto dRGBvalue = leftRGB.at<cv::Vec3b>(r, c);
			auto pdDispairty = SideConsistency::CalcPd(d, c);
			assert(pdDispairty >= 0 && pdDispairty < cols);

			auto pdRGBvalue = rightRGB.at<cv::Vec3b>(r, pdDispairty);

			// Calculate AD cost
			T ADcost = (fabsf(T(dRGBvalue[0]) - T(pdRGBvalue[0])) + fabsf(T(dRGBvalue[1]) - T(pdRGBvalue[1])) + fabsf(T(dRGBvalue[2]) - T(pdRGBvalue[2]))) / 3.f;
			auto ADcensusCost = Rho(ADcost, m_config.adLambda);

			// Calculate census cost

			// Census pixel rounding, Left census ROI location 
			leftRoi.x = c - cwHalfDim;
			rightRoi.y = leftRoi.y = r - chHalfDim;
			// Right census ROI, since the right window is sliding over the image width, we have to handle the cases where is sliding window is out of image scope
			rightRoi.x = pdDispairty - cwHalfDim;

			auto index = r * cols;
			auto xorRes = censusImgLeft[index + c] ^ censusImgRight[index + pdDispairty];

			// Return the number of bits set to true 
			auto censusCost = xorRes.count();
			ADcensusCost += Rho(censusCost, m_config.censusLambda);

			// Finalize 
			dispairtyCostL2R.at<float>(r, c) = ADcensusCost;
		}
	}
}


///////////////////////////////////////////////////////////// Calculate aggregated regions

template <int cw, int ch>
template <typename T>
inline void StereoMatching<cw, ch>::CalcAggregatedRegionsVertical(int c_origin, int r_origin, int rShift, int maxPixelDistance, int cols,
	T Tau1, T Tau2, int L1square, int L2square, const cv::Vec3b& pixelCenterRGBvalue, const Mat& rgb,
	vector<tuple<cv::Point2i, cv::Point2i>>& aggregationRegion, bool& isContinue)
{
	cv::Point2i left, right;
	bool rightContinue, leftContinue;
	// Right aggregation, Aggregate from middle (center) pixel to right - inclusive (r',c)
	{
		bool isContinue = true;
		auto cLimit = std::min(c_origin + maxPixelDistance, cols), cStart = c_origin, cShiftRight = cStart;
		Vec3b RGBvalueShiftedPre;
		for (; isContinue && cShiftRight < cLimit; cShiftRight++)
		{
			CalcAggregatedRegionsVerticalLine<T>(cShiftRight, cStart, c_origin, r_origin, rShift, Tau1, Tau2, L1square, L2square,
				pixelCenterRGBvalue, rgb, RGBvalueShiftedPre, isContinue);
		}
		// cShiftRight has failed the test, therefor we'd accept the previous pixel
		cShiftRight--;
		right.x = cShiftRight;
		right.y = rShift;

		// In case no new element was add, we shall stop the aggregation step
		rightContinue = cShiftRight != cStart;
	}


	// Left aggregation, Aggregate from middle (center) pixel to left - without (r,c)
	{
		bool isContinue = true;
		auto cLimit = std::max(c_origin - maxPixelDistance, 0), cStart = c_origin - 1, cShiftLeft = cStart;
		Vec3b RGBvalueShiftedPre;
		for (; isContinue && cShiftLeft > cLimit; cShiftLeft--)
		{
			CalcAggregatedRegionsVerticalLine<T>(cShiftLeft, cStart, c_origin, r_origin, rShift, Tau1, Tau2, L1square, L2square,
				pixelCenterRGBvalue, rgb, RGBvalueShiftedPre, isContinue);
		}
		cShiftLeft++;
		left.x = cShiftLeft;
		left.y = rShift;

		leftContinue = cShiftLeft != cStart && cStart >= 0;
	}

	isContinue = rightContinue || leftContinue;
	if (isContinue)
	{
		aggregationRegion.push_back(make_tuple(left, right));
	}
}


template <int cw, int ch>
template <typename T>
inline void StereoMatching<cw, ch>::CalcAggregatedRegionsVerticalLine(int cShift, int cStart, int c_origin, int r_origin, int rShift, T Tau1, T Tau2, int L1square, int L2square,
	const cv::Vec3b& pixelCenterRGBvalue, const Mat& rgb,
	cv::Vec3b& RGBvalueShiftedPre, bool& isContinue)
{
	// Follow the three rules from the article 
	auto RGBvalueShifted = rgb.at<cv::Vec3b>(rShift, cShift);

	auto DcResult = Dc<T>(pixelCenterRGBvalue, RGBvalueShifted);

	// First rule check - two conditions 
	isContinue = DcResult < Tau1;

	if (cShift != cStart)
	{
		// The condition means, this is not the first pixel. e.g RGBvalueShiftedPre is exist
		auto DcPre = Dc<T>(pixelCenterRGBvalue, RGBvalueShiftedPre);
		isContinue = DcPre < Tau1;
	}

	// 2nd rule 
	auto dx = cShift - c_origin;
	auto dy = rShift - r_origin;
	auto Ds_square = dx * dx + dy * dy;
	isContinue &= Ds_square < L1square;

	// 3rd rule 
	if (L2square < Ds_square)
	{
		isContinue &= DcResult < Tau2;
	}

	// set the current pixel as pre pixel for next iteration 
	RGBvalueShiftedPre = RGBvalueShifted;
}


template <int cw, int ch>
template <typename T>
inline T StereoMatching<cw, ch>::Dc(const cv::Vec3b& a, const cv::Vec3b& b)
{
	return std::max(std::max(fabsf(T(a[0]) - T(b[0])), fabsf(T(a[1]) - T(b[1]))), fabsf(T(a[2]) - T(b[2])));
}


///////////////////////////////////////////////////////////// Calculate Cross based aggregation 

template <int cw, int ch>
template <typename T>
bool StereoMatching<cw, ch>::DebugMemorySet(cv::Mat& m)
{
	m.setTo(GetMaxCostValue<T>());
	return true;
}

///////////////////////////////////////////////////////////// Scan line optimization 

///////////////////////////////////////////////////////////// CreateDispairtyImage


template <int cw, int ch>
void StereoMatching<cw, ch>::CreateDispairtyImage(std::vector<cv::Mat>& cost, cv::Mat& dispairtyImg, std::vector<float>& costValues)
{
	auto cols = cost[0].cols;
	auto rows = cost[0].rows;

	dispairtyImg.create(rows, cols, CV_32FC1);
	costValues.resize(m_config.DisparityRange);

	for (auto r = 0; r < rows; r++)
	{
		for (auto c = 0; c < cols; c++)
		{
			float minValue = GetMaxCostValue<>();
			int minLoc;
			for (auto d = 0; d < m_config.DisparityRange; d++)
			{
				float value = cost[d].at<float>(r, c);
				if (value < minValue)
				{
					minValue = value;
					minLoc = d;
				}
			}
			assert(minLoc >= 0 && minLoc < m_config.DisparityRange);
			dispairtyImg.at<float>(r, c) = minLoc;
		}
	}
}


template <int cw, int ch>
void StereoMatching<cw, ch>::OutlierDetections()
{
	float fDisparityRange = float(m_config.DisparityRange);

	// Outliers detections 
	auto cols = m_dispairityImgL2R.cols;
	auto rows = m_dispairityImgL2R.rows;

	m_outlierMap.create(rows, cols, CV_8UC1);
	m_outlierMap.setTo(valid);

	for (auto r = 0; r < rows; r++)
	{
		for (auto c = 0; c < cols; c++)
		{
			// Assume that all pixels are valid 
			auto l2r_d = m_dispairityImgL2R.at<float>(r, c);
			auto l2r_pd = LeftToRightConsistency::CalcPd((int)l2r_d, c);

			auto r2l_d = m_dispairityImgR2L.at<float>(r, l2r_pd);
			auto r2l_expected_column_at_l2r = RightToLeftConsistency::CalcPd((int)r2l_d, l2r_pd);

			if (c != r2l_expected_column_at_l2r)
			{
				// Mismatched 
				m_outlierMap.at<uchar>(r, c) = mismach;
			}
		}
	}
}


template <int cw, int ch>
void StereoMatching<cw, ch>::IterativeRegionVoting()
{
	// Iterative region voting 
	auto cols = m_dispairityImgL2R.cols, rows = m_dispairityImgL2R.rows;

	auto& hist = m_histogramMat;
	m_histogramMat.create(1, m_config.DisparityRange, CV_32SC1);
	int* pHistogram = (int*)m_histogramMat.data;

	for (auto i = 0; i < m_config.num_iterative_region_voting; i++)
	{
		for (auto r = 0; r < rows; r++)
		{
			for (auto c = 0; c < cols; c++)
			{
				if (m_outlierMap.at<uchar>(r, c) == mismach)
				{
					memset(pHistogram, 0, sizeof(int) * m_config.DisparityRange);
					const auto& aggreRegion = m_aggregatedRegionsLeft[r * cols + c];
					// Sp - denote as the number of reliable pixels 
					int Sp = 0;

					// build an histogram with the valid bins 
					for (const auto& v : aggreRegion)
					{
						// aggregated coordinates reside in range [0,N], the integral image is in range [0,N+1], therefor, we must change the start and end coordinates according to the following convention, [start', end'] --> [start, end + 1]
						const auto& from = std::get<0>(v);
						const auto& to = std::get<1>(v);

						auto y = to.y;
						for (auto x = from.x; x < to.x; x++)
						{
							if (m_outlierMap.at<uchar>(y, x))
							{
								int dLoc = int(m_dispairityImgL2R.at<float>(y, x));
								pHistogram[dLoc]++;
								++Sp;
							}
						}

						//  Find the max histogram bin 
						double minVal, Hp_maxVal;
						Point minLoc, maxLoc;
						minMaxLoc(hist, &minVal, &Hp_maxVal, &minLoc, &maxLoc);

						// Hp - denote as the highest bin value 
						if ((Sp > m_config.Tau_S) && ((float(Hp_maxVal) / float(Sp)) > m_config.Tau_h))
						{
							m_dispairityImgL2R.at<float>(r, c) = maxLoc.x;
							m_outlierMap.at<uchar>(r, c) = valid;
						}
					}
				}
			}
		}
	}
}


template <int cw, int ch>
void StereoMatching<cw, ch>::SubPixelEnhancement()
{
	auto cols = m_dispairityImgL2R.cols;
	auto rows = m_dispairityImgL2R.rows;
	float dRangeMinusOne = m_config.DisparityRange - 1;

	//parallel_for(int(0), rows, [&](int r)
	for (auto r = 0; r < rows; r++)
	{
		for (auto c = 0; c < cols; c++)
		{
			if (m_outlierMap.at<uchar>(r, c) == mismach)
			{
				auto d = m_dispairityImgL2R.at<float>(r, c);

				if (d == 0 || d == dRangeMinusOne)
					continue;

				auto cost_d = m_costL2R[d].at<float>(r, c);
				auto cost_d_plusOne = m_costL2R[d + 1].at<float>(r, c);
				auto cost_d_minusOne = m_costL2R[d - 1].at<float>(r, c);

				if (cost_d_plusOne == GetMaxCostValue() || cost_d_minusOne == GetMaxCostValue())
					continue;

				auto val = -1 * ((cost_d_plusOne - cost_d_minusOne) / (2 * (cost_d_plusOne + cost_d_minusOne - 2 * cost_d)));

				m_dispairityImgL2R.at<float>(r, c) += val;

				assert(m_dispairityImgL2R.at<float>(r, c) >= 0 && m_dispairityImgL2R.at<float>(r, c) <= dRangeMinusOne);

				m_outlierMap.at<uchar>(r, c) = valid;
			}
		}
	}
	//);
}


template <int cw, int ch>
void StereoMatching<cw, ch>::SetResultToUsersData()
{
	if (m_config.NumPyramids)
	{
		// Pyramid operation may cause a differnt size from origianl image, therfor use resize 
		/*Mat dst;
		auto& src = m_dispairityImgL2R;
		for (auto i = 0; i < m_config.NumPyramids; i++)
		{
		if (i != 0)
		src = dst;

		pyrUp(src, dst);
		}
		assert(size(m_dispairyImgL2R_org) == size(dst));
		dst.copyTo(m_dispairyImgL2R_org);*/

		resize(m_dispairityImgL2R, m_dispairyImgL2R_org, m_dispairyImgL2R_org.size(), 0, 0, INTER_LANCZOS4);
	}
	else
	{
		m_dispairityImgL2R.copyTo(m_dispairyImgL2R_org);
	}
}
