#pragma once


#include "StereoMaching_export.h"
#include <matrix.h>

// Matlab interface 
STEREOMATCHING_EXPORTS_API void* CreateObj();

STEREOMATCHING_EXPORTS_API void DeleteObj(void* obj);

STEREOMATCHING_EXPORTS_API void ComputeStereoMatchingMatlab(void* obj, const mxArray* leftImg, const mxArray* rightImg, mxArray* dispairyMapL2R, int rows, int cols);

STEREOMATCHING_EXPORTS_API void* SetParams(void* obj,
	// General params 
	double NumPyramids, double DisparityRange,
	// AD census params 
	double ch, double cw, double censusLambda, double adLambda,
	// Cost aggregation 
	double isApplyCostAggregation,
	double L1, double L2, double Tau1, double Tau2, double num_cost_aggregation_iterations,
	// Scan line optimization 
	double isApplyScanlineOptimization,
	double Pai1, double Pai2, double Tau_so,
	// Disparity Refinement
	double isApplyDisparityRefinement,
	double Tau_h, double Tau_S, double num_iterative_region_voting);