#include "stdafx.h"
#include "StereoMatchingFactory.h"
#include "StereoMatching.h"


IStereoMatching* StereoMatchingFactory::CreateStereoMatchingObject(const StereoMatchingConfig& config)
{
	return CreateComputeStereoMatching(config);
}


IStereoMatching* StereoMatchingFactory::CreateComputeStereoMatching(const StereoMatchingConfig& config)
{
#define CreateComputeStereoMatchingMaccroCW(ch) 	case ch: return CreateComputeStereoMatchingCW<ch>(config);

	auto chRange = std::min(std::max(1, config.ch), 15); 
	switch (chRange)
	{		
		CreateComputeStereoMatchingMaccroCW(1);
		CreateComputeStereoMatchingMaccroCW(2);
		CreateComputeStereoMatchingMaccroCW(3);
		CreateComputeStereoMatchingMaccroCW(4);
		CreateComputeStereoMatchingMaccroCW(5);
		CreateComputeStereoMatchingMaccroCW(6);
		
		CreateComputeStereoMatchingMaccroCW(7);
		CreateComputeStereoMatchingMaccroCW(8);
		CreateComputeStereoMatchingMaccroCW(9);
		
		CreateComputeStereoMatchingMaccroCW(10);
		CreateComputeStereoMatchingMaccroCW(11);
		CreateComputeStereoMatchingMaccroCW(12);
		CreateComputeStereoMatchingMaccroCW(13);
		CreateComputeStereoMatchingMaccroCW(14);
		CreateComputeStereoMatchingMaccroCW(15);
	
		/*CreateComputeStereoMatchingMaccroCW(16);
		CreateComputeStereoMatchingMaccroCW(16);
		CreateComputeStereoMatchingMaccroCW(17);
		CreateComputeStereoMatchingMaccroCW(18);
		CreateComputeStereoMatchingMaccroCW(19);

		CreateComputeStereoMatchingMaccroCW(20);
		CreateComputeStereoMatchingMaccroCW(21);
		CreateComputeStereoMatchingMaccroCW(22);
		CreateComputeStereoMatchingMaccroCW(23);
		CreateComputeStereoMatchingMaccroCW(24);
		CreateComputeStereoMatchingMaccroCW(25);
		CreateComputeStereoMatchingMaccroCW(26);
		CreateComputeStereoMatchingMaccroCW(27);
		CreateComputeStereoMatchingMaccroCW(28);
		CreateComputeStereoMatchingMaccroCW(29);

		CreateComputeStereoMatchingMaccroCW(30);
		CreateComputeStereoMatchingMaccroCW(31);
		CreateComputeStereoMatchingMaccroCW(32);
		CreateComputeStereoMatchingMaccroCW(33);
		CreateComputeStereoMatchingMaccroCW(34);
		CreateComputeStereoMatchingMaccroCW(35);
		CreateComputeStereoMatchingMaccroCW(36);
		CreateComputeStereoMatchingMaccroCW(37);
		CreateComputeStereoMatchingMaccroCW(38);
		CreateComputeStereoMatchingMaccroCW(39);

		CreateComputeStereoMatchingMaccroCW(40);
		CreateComputeStereoMatchingMaccroCW(41);
		CreateComputeStereoMatchingMaccroCW(42);
		CreateComputeStereoMatchingMaccroCW(43);
		CreateComputeStereoMatchingMaccroCW(44);
		CreateComputeStereoMatchingMaccroCW(45);
		CreateComputeStereoMatchingMaccroCW(46);
		CreateComputeStereoMatchingMaccroCW(47);
		CreateComputeStereoMatchingMaccroCW(48);
		CreateComputeStereoMatchingMaccroCW(49);

		CreateComputeStereoMatchingMaccroCW(50);
		CreateComputeStereoMatchingMaccroCW(51);
		CreateComputeStereoMatchingMaccroCW(52);
		CreateComputeStereoMatchingMaccroCW(53);
		CreateComputeStereoMatchingMaccroCW(54);
		CreateComputeStereoMatchingMaccroCW(55);
		CreateComputeStereoMatchingMaccroCW(56);
		CreateComputeStereoMatchingMaccroCW(57);
		CreateComputeStereoMatchingMaccroCW(58);
		CreateComputeStereoMatchingMaccroCW(59);
		CreateComputeStereoMatchingMaccroCW(60);*/
	}

#undef CreateComputeStereoMatchingMaccroCW(ch)

	return nullptr;
}


template <int ch>
IStereoMatching* StereoMatchingFactory::CreateComputeStereoMatchingCW(const StereoMatchingConfig& config)
{
#define CreateComputeStereoMatchingMaccro(cw) 	case cw: return new StereoMatching<cw, ch>(config);

	auto cwRange = std::min(std::max(1, config.ch), 15);
	switch (cwRange)
	{
		CreateComputeStereoMatchingMaccro(1);
		CreateComputeStereoMatchingMaccro(2);
		CreateComputeStereoMatchingMaccro(3);
		CreateComputeStereoMatchingMaccro(4);
		CreateComputeStereoMatchingMaccro(5);
		CreateComputeStereoMatchingMaccro(6);

		CreateComputeStereoMatchingMaccro(7);
		CreateComputeStereoMatchingMaccro(8);
		CreateComputeStereoMatchingMaccro(9);
		
		CreateComputeStereoMatchingMaccro(10);
		CreateComputeStereoMatchingMaccro(11);
		CreateComputeStereoMatchingMaccro(12);
		CreateComputeStereoMatchingMaccro(13);
		CreateComputeStereoMatchingMaccro(14);
		CreateComputeStereoMatchingMaccro(15);
		
		/*CreateComputeStereoMatchingMaccro(16);
		CreateComputeStereoMatchingMaccro(17);
		CreateComputeStereoMatchingMaccro(18);
		CreateComputeStereoMatchingMaccro(19);

		CreateComputeStereoMatchingMaccro(20);
		CreateComputeStereoMatchingMaccro(21);
		CreateComputeStereoMatchingMaccro(22);
		CreateComputeStereoMatchingMaccro(23);
		CreateComputeStereoMatchingMaccro(24);
		CreateComputeStereoMatchingMaccro(25);
		CreateComputeStereoMatchingMaccro(26);
		CreateComputeStereoMatchingMaccro(27);
		CreateComputeStereoMatchingMaccro(28);
		CreateComputeStereoMatchingMaccro(29);

		CreateComputeStereoMatchingMaccro(30);
		CreateComputeStereoMatchingMaccro(31);
		CreateComputeStereoMatchingMaccro(32);
		CreateComputeStereoMatchingMaccro(33);
		CreateComputeStereoMatchingMaccro(34);
		CreateComputeStereoMatchingMaccro(35);
		CreateComputeStereoMatchingMaccro(36);
		CreateComputeStereoMatchingMaccro(37);
		CreateComputeStereoMatchingMaccro(38);
		CreateComputeStereoMatchingMaccro(39);

		CreateComputeStereoMatchingMaccro(40);
		CreateComputeStereoMatchingMaccro(41);
		CreateComputeStereoMatchingMaccro(42);
		CreateComputeStereoMatchingMaccro(43);
		CreateComputeStereoMatchingMaccro(44);
		CreateComputeStereoMatchingMaccro(45);
		CreateComputeStereoMatchingMaccro(46);
		CreateComputeStereoMatchingMaccro(47);
		CreateComputeStereoMatchingMaccro(48);
		CreateComputeStereoMatchingMaccro(49);

		CreateComputeStereoMatchingMaccro(50);
		CreateComputeStereoMatchingMaccro(51);
		CreateComputeStereoMatchingMaccro(52);
		CreateComputeStereoMatchingMaccro(53);
		CreateComputeStereoMatchingMaccro(54);
		CreateComputeStereoMatchingMaccro(55);
		CreateComputeStereoMatchingMaccro(56);
		CreateComputeStereoMatchingMaccro(57);
		CreateComputeStereoMatchingMaccro(58);
		CreateComputeStereoMatchingMaccro(59);
		CreateComputeStereoMatchingMaccro(60);*/
	}

#define CreateComputeStereoMatchingMaccro(cw) 

	return nullptr;
}


void StereoMatchingFactory::SetConfig(IStereoMatching*& obj, const StereoMatchingConfig& config)
{
	assert(obj != nullptr);

	auto& preConfig = obj->GetConfig();

	//if (preConfig.cw != config.cw || preConfig.ch != config.ch)
	{
		delete obj;
		obj = CreateStereoMatchingObject(config);
	}
//	else
//	{
//		obj->SetConfig(config);
//	}
}
