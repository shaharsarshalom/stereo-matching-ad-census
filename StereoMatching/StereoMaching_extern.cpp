#pragma once

#include "stdafx.h"
#include "StereoMaching_export.h"
#include "StereoMatchingConfig.h"
#include "StereoMatchingFactory.h"
#include "IStereoMatching.h"
#include <matrix.h>
#include <iostream>


extern "C"
{
	STEREOMATCHING_EXPORTS_API void* CreateObj()
	{
		try
		{
			auto obj = StereoMatchingFactory::CreateStereoMatchingObject();
			auto objType = static_cast<void*>(obj);

			return objType;
		}
		catch (const std::exception& e)
		{
			// this executes if f() throws std::logic_error (base class rule)
			std::cout << e.what(); // information from length_error printed
		}
		catch (...)
		{

		}
	}


	STEREOMATCHING_EXPORTS_API void DeleteObj(void* obj)
	{
		auto objType = static_cast<IStereoMatching*>(obj);
		delete objType;
	}

	STEREOMATCHING_EXPORTS_API void ComputeStereoMatchingMatlab(void* obj, const mxArray* leftImg, const mxArray* rightImg, mxArray* dispairyMapL2R, int rows, int cols)
	{
		try
		{
			auto objType = static_cast<IStereoMatching*>(obj);

			void* leftImgPtr = mxGetPr(leftImg);
			void* rightImgPtr = mxGetPr(rightImg);
			void* dispairyMapL2RPtr = mxGetPr(dispairyMapL2R);

			assert(leftImgPtr != nullptr && rightImgPtr != nullptr && dispairyMapL2RPtr != nullptr);

			objType->ComputeStereoMatchingMatlab((uchar*)leftImgPtr, (uchar*)rightImgPtr, (float*)dispairyMapL2RPtr, rows, cols);
		}
		catch (const std::exception& e)
		{
			// this executes if f() throws std::logic_error (base class rule)
			std::cout << e.what(); // information from length_error printed
		}
		catch (...)
		{

		}
	}


	STEREOMATCHING_EXPORTS_API void* SetParams(void* obj,
		// General params 
		double NumPyramids, double DisparityRange,
		// AD census params 
		double ch, double cw, double censusLambda, double adLambda,
		// Cost aggregation 
		double isApplyCostAggregation,
		double L1, double L2, double Tau1, double Tau2, double num_cost_aggregation_iterations,
		// Scan line optimization 
		double isApplyScanlineOptimization,
		double Pai1, double Pai2, double Tau_so, 
		// Disparity Refinement
		double isApplyDisparityRefinement,
		double Tau_h, double Tau_S, double num_iterative_region_voting)
	{
		try
		{
			auto objType = static_cast<IStereoMatching*>(obj);

			StereoMatchingConfig config;

			config.NumPyramids = NumPyramids;
			config.DisparityRange = DisparityRange; 

			config.ch = ch;
			config.cw = cw;
			config.censusLambda = censusLambda;
			config.adLambda = adLambda;

			config.isApplyCostAggregation = isApplyCostAggregation != 0;
			config.L1 = L1;
			config.L2 = L2;
			config.Tau1 = Tau1;
			config.Tau2 = Tau2;
			config.num_cost_aggregation_iterations = num_cost_aggregation_iterations;

			config.isApplyScanlineOptimization = isApplyScanlineOptimization != 0;
			config.Pai1 = Pai1;
			config.Pai2 = Pai2;
			config.Tau_so = Tau_so;

			config.isApplyDisparityRefinement = isApplyDisparityRefinement != 0;
			config.Tau_h = Tau_h;
			config.Tau_S = Tau_S;
			config.num_iterative_region_voting = num_iterative_region_voting;
			
			StereoMatchingFactory::SetConfig(objType, config);

			return (void*)objType;
		}
		catch (const std::exception& e)
		{
			// this executes if f() throws std::logic_error (base class rule)
			std::cout << e.what(); // information from length_error printed
		}
		catch (...)
		{

		}
	}

}
