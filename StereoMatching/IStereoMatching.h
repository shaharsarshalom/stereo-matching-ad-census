#pragma once

#include "stdafx.h"
#include <opencv2\core.hpp>
#include "StereoMaching_export.h"
#include "StereoMatchingConfig.h"


class STEREOMATCHING_EXPORTS_API IStereoMatching
{
public:
	virtual ~IStereoMatching() = 0 {};

	virtual void ComputeStereoMatching(const cv::Mat& left, const cv::Mat& right, cv::Mat& dispairyMap) = 0;
	virtual void ComputeStereoMatchingMatlab(const uchar* leftPtr, const uchar* rightPtr, float* disparityMapPtr, int rows, int cols) = 0;

	virtual const StereoMatchingConfig& GetConfig() = 0;
	virtual void SetConfig(const StereoMatchingConfig& config) = 0;
};