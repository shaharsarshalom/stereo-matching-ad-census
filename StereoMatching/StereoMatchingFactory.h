#pragma once

#include "stdafx.h"
#include "IStereoMatching.h"
#include "StereoMatchingConfig.h"


class STEREOMATCHING_EXPORTS_API StereoMatchingFactory
{
public:
	static IStereoMatching* CreateStereoMatchingObject(const StereoMatchingConfig& config = StereoMatchingConfig());

	static void SetConfig(IStereoMatching*& obj, const StereoMatchingConfig& config);

private: 

	static IStereoMatching* CreateComputeStereoMatching(const StereoMatchingConfig& config);

	template <int ch>
	static IStereoMatching* CreateComputeStereoMatchingCW(const StereoMatchingConfig& config);
};

