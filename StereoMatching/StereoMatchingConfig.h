#pragma once

#include "stdafx.h"
#include <string>


struct StereoMatchingConfig
{	
	// Number of pyramids for all process 
	int NumPyramids = 0;

	// Half disparity range 
	int DisparityRange = 20;
	// Full disparity rage is consider as num labels 

	// Census window size
	int ch = 9;
	int cw = 7;
	int censusLambda = 30;
	int adLambda = 10;

	// Cost aggregation parameters
	bool isApplyCostAggregation = true;
	int L1 = 34;
	int L2 = 17;
	int Tau1 = 20;
	int Tau2 = 6;
	int num_cost_aggregation_iterations = 4;

	// Scanline optimization params
	bool isApplyScanlineOptimization = true;
	float Pai1 = 1.0;
	float Pai2 = 3.0;
	int Tau_so = 15;

	// Disparity refinement params
	bool isApplyDisparityRefinement = true;
	// Iterative region voting scheme 
	float Tau_h = 0.4f;
	int Tau_S = 20;
	int num_iterative_region_voting = 5;

	// Debug parameters
	bool DebugMode = false;

	std::string DebugPath = "C:\\SM\\";
	std::string DebugExt = "bmp";
};


