#include "StereoMatchingUT.h"
#include <StereoMatching.h>
#include <StereoMatchingFactory.h>
#include <string>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <Stopper.h>
#include <experimental/filesystem>
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;


void StereoMatchingUT::Test1()
{
	StereoMatchingConfig config;

	// string path = "C:/Users/Shahar Sar Shalom .DESKTOP-OF8R54E/Dropbox/Shahar_FinalProjectFinalWork/AdvenceProjectInComputerSience/Matlab/DataSet/Middlebury Stereo Evaluation - Version 2/Tsukuba/";

	// Cones 
	// string path = "C:/Users/Shahar Sar Shalom .DESKTOP-OF8R54E/Dropbox/Shahar_FinalProjectFinalWork/AdvenceProjectInComputerSience/Matlab/DataSet/Middlebury Stereo Evaluation - Version 2/cones/";

	// Teddy
	//string path = "C:/Users/Shahar Sar Shalom .DESKTOP-OF8R54E/Dropbox/Shahar_FinalProjectFinalWork/AdvenceProjectInComputerSience/Matlab/DataSet/Middlebury Stereo Evaluation - Version 2/Teddy/";
	//config.DisparityRange = 59;

	// Art 
	//string path = "C:/Users/Shahar Sar Shalom .DESKTOP-OF8R54E/Dropbox/Shahar_FinalProjectFinalWork/AdvenceProjectInComputerSience/Matlab/DataSet/Middlebury Stereo Evaluation - Version 3/MiddEval3-data-Q/MiddEval3/trainingQ/ArtL/";
	//config.DisparityRange = 64;

	// Playtable
	//string path = "C:/Users/Shahar Sar Shalom .DESKTOP-OF8R54E/Dropbox/Shahar_FinalProjectFinalWork/AdvenceProjectInComputerSience/Matlab/DataSet/Middlebury Stereo Evaluation - Version 3/MiddEval3-data-Q/MiddEval3/trainingQ/Playtable/";
	//config.DisparityRange = 73;

	//Motorcycle
	//string path = "C:/Users/Shahar Sar Shalom .DESKTOP-OF8R54E/Dropbox/Shahar_FinalProjectFinalWork/AdvenceProjectInComputerSience/Matlab/DataSet/Middlebury Stereo Evaluation - Version 3/MiddEval3-data-Q/MiddEval3/trainingQ/Motorcycle/";
	//config.DisparityRange = 70;

	//Piano
	string path = "C:/Users/Shahar Sar Shalom .DESKTOP-OF8R54E/Dropbox/Shahar_FinalProjectFinalWork/AdvenceProjectInComputerSience/Matlab/DataSet/Middlebury Stereo Evaluation - Version 3/MiddEval3-data-Q/MiddEval3/trainingQ/Piano/";
	config.DisparityRange = 65;

	string imgLeftPath = path + "im0.png", imgRightPath = path + "im1.png";

	GenericTest(imgLeftPath, imgRightPath, config, "t1");
}


void StereoMatchingUT::Test2()
{
	// TIF IMAGES DOESN'T WORK IN OUR SYSTEM, PLEASE DON'T WORK WITH TIF !!!! TEST 2 IS CANCELLED 
	string path = "C:/Users/Shahar Sar Shalom .DESKTOP-OF8R54E/Dropbox/Shahar_FinalProjectFinalWork/AdvenceProjectInComputerSience/Matlab/DataSet/Mars Orbital Camera/";
	string imgLeftPath = path + "im0.tif", imgRightPath = path + "im1.tif";

	StereoMatchingConfig config;
	config.DisparityRange = 90;

	GenericTest(imgLeftPath, imgRightPath, config, "t2");
}


void StereoMatchingUT::GenericTest(const std::string& imgLeftPath, const std::string& imgRightPath, const StereoMatchingConfig& config, std::string fileName)
{
	if (!std::experimental::filesystem::exists(imgLeftPath.c_str())) throw;
	if (!std::experimental::filesystem::exists(imgRightPath.c_str())) throw;

	Mat imgLeft = imread(imgLeftPath, CV_LOAD_IMAGE_COLOR), imgRight = imread(imgRightPath, CV_LOAD_IMAGE_COLOR), dispairyMapL2R;

	auto* stereoMatching = StereoMatchingFactory::CreateStereoMatchingObject(config);
	auto numIterations = 50; 
	
	for (auto i = 0; i < numIterations; i++)
	{
		CStopper::getInstance().Start();

		stereoMatching->ComputeStereoMatching(imgLeft, imgRight, dispairyMapL2R);

		CStopper::getInstance().Stop();
	}
	CStopper::getInstance().PrintCounter();

	if (true)
	{
		CImageUtility::dbg_writeImg(dispairyMapL2R, m_debugPath + fileName + ".jpg");
	}

	delete stereoMatching;

	//cin.get();
}