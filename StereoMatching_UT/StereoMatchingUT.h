#pragma once

#include <string>
#include <StereoMatchingConfig.h>


class StereoMatchingUT
{
public:
	StereoMatchingUT() {};
	~StereoMatchingUT() {};

	void GenericTest(const std::string& imgLeftPath, const std::string& imgRightPath, const StereoMatchingConfig& config, std::string fileName);

	void RunTests()
	{
		Test1();
	}

private: 
	void Test1();

	// TIF IMAGES DOESN'T WORK IN OUR SYSTEM, PLEASE DON'T WORK WITH TIF !!!! TEST 2 IS CANCELLED 
	void Test2();

	std::string m_debugPath = "C:/Users/Shahar Sar Shalom .DESKTOP-OF8R54E/Dropbox/Shahar_FinalProjectFinalWork/AdvenceProjectInComputerSience/";
};

