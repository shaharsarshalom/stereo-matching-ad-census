# Stereo-Matching-AD-Census

This project implements a Stereo Matching algorithm called the AD-Census, which represented in the article [Building an Accurate Stereo Matching System on Graphics Hardware](https://xing-mei.github.io/files/adcensus.pdf).

The algorithm was implemented in C++ using visual studio in windows (I ended out porting the algorithm to matlab for further analysis).

Here is an exmaple for the input and output image (disparity map) from the algorithm-

![input image](Images/ConvertRGBFromMatlabToOpencv.png)

![disparity map - output image](Images/ComputeStereoMatching.png)


This project was part of the Ms.c studies in the Open University by the supervision of Dr Azaria Cohen. 

The project summery document (Hebrew) is also attached to the git.

Best 

Shahar